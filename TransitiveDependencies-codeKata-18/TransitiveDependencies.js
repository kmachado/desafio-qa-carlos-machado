/**
 * Created by carlos on 9/10/2015.
 */

/*
* this is a classic problem of  Breadth-first search over a graph.
*
* */
TransitiveDependencies = function(){
    this.queue = [];

    //array to place the list of elements and its dependencies
    this.dependencies = [];


    this.add_direct = function(item,dependencies){
        var _this = this;

        //assign default value [] for the first time adding an element dependencies
        _this.dependencies[item] = _this.dependencies[item] || [];

        //adding to the list of dependencies of the element :item just those
        // dependencies that are not added yet
        _this.dependencies[item] = _this.dependencies[item].
            concat(dependencies.filter(function(e){
                return -1 === _this.dependencies[item].indexOf(e)
            }));
    };

    this.dependencies_for = function(item){
        var _this = this,
            currentElement = item,
            transitiveDependencies = [],
            i;
        //checking if the element :item exist and has direct dependencies at least
        if (_this.dependencies[currentElement]){
            i = 0;
            //adding the :item's direct dependencies to de queue
            _this.queue = _this.dependencies[currentElement];

            //while there is at leas an element in the queue that havent been visited yet
            while (_this.queue.length >= i ) {
                //checking if the element :currentElement exist and has direct dependencies
                if (_this.dependencies[currentElement]){
                    _this.queue = _this.queue.
                        concat(_this.dependencies[currentElement].filter(function(e){
                            //adding to the queue just the elements that are not
                            //visited yet and are not the root element
                            return (-1 === _this.queue.indexOf(e) && e !== item)
                        }));
                }
                //set currentElement the next element to visit on the queue
                currentElement = _this.queue[i++];
            }

            //saving dependencies inside qthe queue to :transitiveDependencies
            transitiveDependencies = _this.queue;

            // to clean the queue for the next execution of this function
            _this.queue = [];
            // return transitiveDependencies as result
            return item + ' => '+ transitiveDependencies.join(' ');
        }else{
            return transitiveDependencies;
        }
    };
};
