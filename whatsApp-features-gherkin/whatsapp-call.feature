Feature: Llamar a contacto

Background:
  Given Estoy en la vista CONTACTOS
  And Existe al  menos un contacto
  Then selecciono un contacto

Scenario: Llamar a un contacto
  Given estoy conectado a internet
  When  selecciono la opcion llamar
  Then se establece la llamada

Scenario: Llamar a un contacto cuando no estoy conectado a internet
  Given no estoy conectado a internet
  When  selecciono la opcion de llamar
  Then aparece un mensaje diciendo que no estoy conectado a internet y que lo intente mas tarde


