Feature: Enviar mensajes a contacto desde la vista de CHAT

Background:
  Given Estoy en la vista CHATS
  And Existe al  menos un contacto
  Then selecciono un contacto

Scenario: Enviar un mensaje a un contacto estando conectado
  Given estoy conectado a internet
  When  escribo el texto del mensaje
  And selecciono la opcion enviar
  Then aparece en el mensaje el icono que indica que ha sido enviado correctamente

Scenario: Enviar un mensaje a un contacto sin conexion a internet
  Given no estoy conectado a internet
  When  escribo el texto del mensaje
  And selecciono la opcion enviar
  Then aparece en el mensaje el icono que indica que est� pendiente de enviar
